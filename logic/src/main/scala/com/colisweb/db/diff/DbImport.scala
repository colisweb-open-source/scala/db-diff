package com.colisweb.db.diff

import com.colisweb.db.diff.db.DbConnection
import com.colisweb.db.diff.files.ReadWrite
import com.colisweb.db.diff.imports.{ImportConfig, ImportLogic}

object DbImport extends App {
  val service :: environment :: inputFiles = args.toList

  val importConfig = ImportConfig(service, environment)
  val dbConnection = DbConnection(importConfig.connection, service, environment)
  val logic        = ImportLogic(dbConnection, importConfig)

  def importTable(name: String): Boolean =
    importConfig.tables.isEmpty || importConfig.tables.map(_.tableName).contains(name)

  val data = inputFiles.flatMap(ReadWrite.readFiles).filter(dr => importTable(dr.tableName))
  data.foreach(logic.writeTable)

}
