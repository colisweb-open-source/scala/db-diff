package com.colisweb.db.diff.db.metadata

import org.slf4j.LoggerFactory
import os.Path

import scala.util.{Failure, Success, Try}

/**
  * Holds metadata (column types, primary and foreign keys) for a single table.
  */
case class TableMetaData(
    tableName: String,
    primaryKeyColumns: Seq[String],
    foreignKeys: Seq[ForeignKey],
    columns: Seq[ColumnDescription]
) {

  def column(name: String): Option[ColumnDescription] = columns.find(_.name.toLowerCase == name.toLowerCase)

}

object TableMetaData {
  import upickle.default._
  val logger = LoggerFactory.getLogger(getClass)

  implicit val readWriter: ReadWriter[TableMetaData] = macroRW[TableMetaData]

  def metadataFolder(service: String, environment: String): Path = os.pwd / service / environment
  def metadataFile(service: String, environment: String): Path   = metadataFolder(service, environment) / "metadata.json"

  def readMetaData(service: String, environment: String): Option[Map[String, TableMetaData]] = {
    val file = metadataFile(service, environment)
    Try {
      val content = os.read(file)
      read[Map[String, TableMetaData]](content)
    } match {
      case Failure(exception) =>
        logger.warn(s"Could not read metadata from $file ($exception)")
        None
      case Success(value)     =>
        logger.info(s"metadata read from $file")
        Some(value)
    }
  }

  def writeMetaData(service: String, environment: String, data: Map[String, TableMetaData]): Unit = {
    os.makeDir.all(metadataFolder(service, environment))
    val file = metadataFile(service, environment)
    os.write.over(file, write(data, indent = 2))
    logger.info(s"metadata written into $file")
  }
}
