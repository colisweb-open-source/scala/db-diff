package com.colisweb.db.diff.db.transformation

import com.colisweb.db.diff.db.DataRows
import com.colisweb.db.diff.db.metadata.ColumnDescription

final case class RenameColumn(source: String, target: String) extends MigrateColumn {
  override def description(rows: DataRows): ColumnDescription = rows.column(source).copy(name = target)

  override def transform(data: Map[String, String]): String = data(source)

  override def oldColumnName: Option[String] = Some(source)
}
