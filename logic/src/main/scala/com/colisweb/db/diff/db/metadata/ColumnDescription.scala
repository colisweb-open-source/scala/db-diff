package com.colisweb.db.diff.db.metadata

import java.sql.{JDBCType, Types}

case class ColumnDescription(name: String, typeName: String, jdbcType: Int, tableName: String) {

  def isArray: Boolean = jdbcType == Types.ARRAY

  def isTimestamp: Boolean = jdbcType == Types.TIMESTAMP

  //When a column is an enum in postgres, jdbcType = VARCHAR and typeName is the enum name (days for example)
  def isPgEnum: Boolean = {
    val varcharTypeNames = List("VARCHAR", "CHARACTER VARYING")
    val isVarcharType    = varcharTypeNames.exists(typeName.toUpperCase.startsWith)
    jdbcType == Types.VARCHAR && !isVarcharType
  }
}

object ColumnDescription {

  def apply(name: String, jdbcType: JDBCType, tableName: String): ColumnDescription =
    ColumnDescription(name, jdbcType.getName, jdbcType.getVendorTypeNumber, tableName)

  def apply(name: String, typeName: String, jdbcType: JDBCType, tableName: String): ColumnDescription =
    ColumnDescription(name, typeName, jdbcType.getVendorTypeNumber, tableName)

  def apply(data: Seq[String]): ColumnDescription =
    ColumnDescription(data.head, data(1), data(2).toInt, data(3))

  import upickle.default._

  implicit val readWriter: ReadWriter[ColumnDescription] = macroRW[ColumnDescription]

}
