package com.colisweb.db.diff.db

import com.github.writethemfirst.Approbation
import com.colisweb.db.diff.db.metadata.ColumnDescription
import com.colisweb.db.diff.exports._
import com.colisweb.db.diff.exports
import com.colisweb.db.diff.imports._
import com.colisweb.db.diff.imports
import org.scalatest.flatspec.FixtureAnyFlatSpec
import org.scalatest.matchers.should.Matchers
import upickle.default._

import java.sql.JDBCType.{ARRAY, INTEGER}

class DbConnectionSpec extends FixtureAnyFlatSpec with Matchers with Approbation with DbSetup {

  val dummyService = "service"

  for (db <- dbs) {
    import db._

    lazy val dbConnection = DbConnection(config, dummyService, name)

    def initDb(): Unit = {
      val f = flyway
      f.clean()
      f.migrate()
    }

    name should "get tables metadata" in { approver =>
      initDb()
      val metadata = dbConnection.metadata.allTables.toMap
      approver.verify(write(metadata, 2))
    }

    val tableName    = "orders"
    def exportConfig = ExportConfig(config)
    def exportLogic  = ExportLogic(dbConnection, exportConfig)

    def exportLogicForTable(
        tables: List[com.colisweb.db.diff.exports.TableConfig],
        tableName: String
    ): ExportLogic#ExportLogicForTable =
      ExportLogic(dbConnection, ExportConfig(config, tables = tables)).ExportLogicForTable(tableName)

    def importLogic(tables: List[com.colisweb.db.diff.imports.TableConfig]): ImportLogic =
      ImportLogic(dbConnection, ImportConfig(config, tables = tables))

    name should "export an empty table" in { _ =>
      initDb()
      val rows = exportLogic.ExportLogicForTable("products").tableContent
      rows.size shouldBe 0
    }

    name should "export an empty table recursively" in { _ =>
      initDb()
      val logic =
        ExportLogic(dbConnection, exportConfig.copy(tables = List(exports.TableConfig("products", followKeys = true))))
      val rows  = logic.ExportLogicForTable("products").readTableRecursively().head
      rows.size shouldBe 0
    }

    name should "export a single table content" in { approver =>
      initDb()
      val rows = exportLogic.ExportLogicForTable(tableName).tableContent
      approver.verify(rows.toJson)
    }

    name should "export a single table content with rows limit" in { approver =>
      initDb()
      val rows =
        exportLogicForTable(List(exports.TableConfig(tableName, maxRowsCount = Some(2))), tableName).tableContent
      approver.verify(rows.toJson)
    }

    name should "detect links between tables" in { approver =>
      initDb()
      approver.verify(prettify(exportLogic.links))
    }

    name should "export recursively content from tables" in { approver =>
      initDb()

      val rows = exportLogicForTable(
        List(
          exports.TableConfig(
            tableName,
            followKeys = true,
            idColumnName = Some("orderid"),
            idValues = Seq("1")
          )
        ),
        tableName
      ).readTableRecursively()
      approver.verify(rows.map(_.toJson))
    }

    // Theses fields must keep lazy initialization or test containers fail with
    // java.lang.IllegalStateException: Mapped port can only be obtained after the container is started

    def enumTypeName = db.config.engine.enumRepresentation.typeName
    def enumJdbcType = db.config.engine.enumRepresentation.jdbcType

    def ordersRows =
      DataRows(
        tableName,
        Vector(
          ColumnDescription("orderid", "serial", 4, tableName),
          ColumnDescription("orderdate", "int4", 91, tableName),
          ColumnDescription("customerid", "serial", 4, tableName),
          ColumnDescription("totalamount", "numeric", 2, tableName),
          ColumnDescription("day_of_week", enumTypeName.getOrElse("days"), enumJdbcType, tableName)
        ),
        Vector()
      )

    def sameIdRow = ordersRows.copy(data = Vector(Vector("1", "2022-01-01", "1", "34.99", "MONDAY")))
    def newIdRow  = ordersRows.copy(data = Vector(Vector("4", "2022-01-01", "1", "34.99", "MONDAY")))

    name should "import data in a single table" in { approver =>
      initDb()
      val rows = DataRows(
        tableName,
        Vector(
          ColumnDescription("orderid", "serial", 4, tableName),
          ColumnDescription("orderdate", "int4", 91, tableName),
          ColumnDescription("customerid", "serial", 4, tableName),
          ColumnDescription("totalamount", "numeric", 2, tableName),
          ColumnDescription("day_of_week", enumTypeName.getOrElse("days"), enumJdbcType, tableName)
        ),
        Vector(Vector("1", "2022-01-01", "1", "34.99", "MONDAY"))
      )

      val importTableConnection =
        importLogic(
          List(imports.TableConfig(tableName = tableName, List("orderid"), DeleteAndCreate))
        )

      importTableConnection.writeTable(rows)

      val after = exportLogicForTable(Nil, tableName).readTableRecursively()
      approver.verify(after.map(_.toJson))
    }

    name should "import data in a single table using create" in { approver =>
      initDb()

      val tableConnection =
        importLogic(
          List(imports.TableConfig(tableName = tableName, idColumns = List("orderid"), idBehaviour = Create))
        )

      assertThrows[FailedImport](tableConnection.writeTable(sameIdRow))

      tableConnection.writeTable(newIdRow)

      val after = exportLogicForTable(Nil, tableName).readTableRecursively()
      approver.verify(after.map(_.toJson))
    }

    name should "import data in a single table using update" in { approver =>
      initDb()

      val tableConnection =
        importLogic(
          List(imports.TableConfig(tableName = tableName, idColumns = List("orderid"), idBehaviour = Update))
        )

      tableConnection.writeTable(sameIdRow)

      val after = exportLogicForTable(Nil, tableName).readTableRecursively()
      approver.verify(after.map(_.toJson))
    }

    name should "delete data in a single table using delete" in { approver =>
      initDb()

      val tableConnection =
        importLogic(
          List(imports.TableConfig(tableName = tableName, idColumns = List("orderid"), idBehaviour = Delete))
        )

      tableConnection.writeTable(sameIdRow)

      val after = exportLogicForTable(Nil, tableName).readTableRecursively()
      approver.verify(after.map(_.toJson))
    }

    name should "change nothing when trying to delete non present data" in { approver =>
      initDb()

      val tableConnection =
        importLogic(
          List(imports.TableConfig(tableName = tableName, idColumns = List("orderid"), idBehaviour = Delete))
        )

      tableConnection.writeTable(newIdRow)

      val after = exportLogicForTable(Nil, tableName).readTableRecursively()
      approver.verify(after.map(_.toJson))
    }

    if (db == postgresVendor) {
      name should "create a valid statement for null and arrays" in { approver =>
        initDb()

        val key = ColumnDescription("key", INTEGER, "map")
        val arr = ColumnDescription("arr", "_varchar", ARRAY, "map")
        val num = ColumnDescription("i", INTEGER, "map")

        val repository = dbConnection.repository

        repository.withConnection { implicit c =>
          repository.createAndInsert(
            DataRows(
              "map",
              Vector(key, arr, num),
              Vector(
                Vector("1", "{one, two, three}", "null"),
                Vector("2", "{}", "11"),
                Vector("3", "{}", "1"),
                Vector("4", "{a,b}", "1")
              )
            )
          )
          repository.update("map", Map(num -> "22", arr -> "{c,d}"), Vector((key, "3")))
          repository.update("map", Map(num -> "null", arr -> "{}"), Vector((key, "4")))
        }

        val logic =
          ExportLogic(DbConnection(config, dummyService, name), ExportConfig(config, Nil)) // force reloading metadata
            .ExportLogicForTable("map")
        approver.verify(logic.readTableRecursively().map(_.toJson))
      }
    }

    name should "read data as a batch iterator" in { approver =>
      initDb()

      val data = dbConnection.repository.selectAll("orders", "1 = 1").aggregate

      approver.verify(data.toJson)
    }

    name should "run any SQL query in the Repository as a batch" in { approver =>
      initDb()

      val data =
        dbConnection.repository
          .sqlSelect("""SELECT sum(totalamount) amount, country
                       | FROM orders JOIN customers ON orders.customerid = customers.customerid
                       | GROUP BY customers.country
                       |""".stripMargin)
          .aggregate

      approver.verify(data.toJson)
    }

    name should "run any SQL query in the Repository with aliases" in { approver =>
      initDb()

      val data =
        dbConnection.repository
          .sqlSelect("""SELECT country as alias_country
                       | FROM customers
                       |""".stripMargin)
          .aggregate

      approver.verify(data.toJson)
    }

    name should "run 2 SQL queries in the Repository as a batch" in { approver =>
      initDb()

      val iterator1 =
        dbConnection.repository.sqlSelect("""SELECT sum(totalamount) amount, country
                                            | FROM orders JOIN customers ON orders.customerid = customers.customerid
                                            | GROUP BY customers.country
                                            |""".stripMargin)

      val iterator2 =
        dbConnection.repository.sqlSelect("""SELECT max(totalamount) amount, country
                                            | FROM orders JOIN customers ON orders.customerid = customers.customerid
                                            | GROUP BY customers.country
                                            |""".stripMargin)

      val data = DataRows.aggregate(List(iterator1.aggregate, iterator2.aggregate))

      approver.verify(data.map(_.toJson))
    }

  }

}
