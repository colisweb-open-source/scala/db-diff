package com.colisweb.db.diff.imports

import com.colisweb.db.diff.db.metadata.ColumnDescription
import com.colisweb.db.diff.db.{DataRows, DbConnection}
import org.slf4j.LoggerFactory

import java.sql.Connection
import scala.collection.immutable.ListMap

final case class ImportLogic(
    dbConnection: DbConnection,
    importConfig: ImportConfig
) {
  import dbConnection._
  val logger = LoggerFactory.getLogger(getClass)

  def writeTable(dataRows: DataRows): Unit = {
    val totalSize = dataRows.size.toDouble
    logger.info(s"processing ${totalSize.toInt} rows for ${dataRows.tableName}")

    var lastPercentValue: Option[Int] = None

    def updatePercentCompletion(rowIndex: Int): Option[Int] = {
      val percentCompletion: Int = ((rowIndex / totalSize) * 100).toInt

      if (lastPercentValue.contains(percentCompletion)) None
      else {
        lastPercentValue = Some(percentCompletion)
        lastPercentValue
      }
    }

    repository.withConnection(implicit connection =>
      dataRows.data.zipWithIndex.foreach {
        case (row, index) =>
          processRow(row, dataRows, updatePercentCompletion(index + 1))
      }
    )
  }

  private def processRow(
      values: Vector[String],
      dataRows: DataRows,
      percentCompletionToPrint: Option[Int]
  )(implicit connection: Connection): Unit = {
    val tableName     = dataRows.tableName
    val tableMetaData = dbConnection.tableMetadata(tableName)

    for {
      tableConf <- importConfig.tables.find(_.tableName == tableName)
      if tableConf.idColumns.nonEmpty
    } {
      val ids: List[String]                                   = tableConf.idColumns
      val columns: Vector[ColumnDescription]                  = dataRows.columns(ids)
      val data: List[String]                                  = ids.map(i => values(dataRows.columnIndex(i)))
      val columnsAndData: Vector[(ColumnDescription, String)] = columns.zip(data)
      val valueAlreadyExists                                  = repository.counts(tableName, columnsAndData) > 0
      (tableConf.idBehaviour, valueAlreadyExists) match {
        case (Create, true)          =>
          throw FailedImport(tableName, columns.map(_.name).mkString(","), data.mkString(","))

        case (DeleteAndCreate, true) =>
          repository.deleteRow(tableName, columnsAndData)
          repository.insertRow(values, dataRows)

        case (Update, true)          =>
          //TODO : ne rien faire si on a pas de colonnes à update (tout est dans la clé primaire)
          val keys                = (tableMetaData.foreignKeys.map(_.name) ++ tableMetaData.primaryKeyColumns).toSet
          val mappedValues        = ListMap.from(dataRows.columns.zip(values))
          val keylessMappedValues = mappedValues.filterNot(m => keys(m._1.name))
          repository.update(tableName, keylessMappedValues, columnsAndData)

        case (Skip, true)            => "."

        case (Delete, _) =>
          repository.deleteRow(tableName, columnsAndData)
        case (_, false)  =>
          repository.insertRow(values, dataRows)
      }

      percentCompletionToPrint.foreach(i => logger.info(s"${tableConf.idBehaviour} completion: $i %"))
    }
  }

}

case class FailedImport(tableName: String, columnName: String, value: String)
    extends RuntimeException(s"a row with $columnName=$value already exists in table $tableName")
