package com.colisweb.db.diff.db

import com.colisweb.db.diff.db.metadata.ColumnDescription
import com.colisweb.db.diff.db.transformation._
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.scalatest.prop.TableDrivenPropertyChecks._
import org.scalatest.prop.TableFor2

import java.sql.JDBCType.{INTEGER, VARCHAR}
import java.time.DayOfWeek

class DataRowsSpec extends AnyFlatSpec with Matchers {

  val oneRow: DataRows = DataRows(
    tableName = "atable",
    columns = Vector(
      ColumnDescription(name = "col1", jdbcType = VARCHAR, tableName = "atable")
    ),
    data = Vector(Vector("data1"), Vector("data2"))
  )

  val manyRows: DataRows = DataRows(
    tableName = "atable",
    columns = Vector(
      ColumnDescription(name = "col1", jdbcType = VARCHAR, tableName = "atable"),
      ColumnDescription(name = "col2", jdbcType = VARCHAR, tableName = "atable")
    ),
    data = Vector(Vector("data1", "1"), Vector("data2", "2"))
  )

  "DataRows" should "rename the table" in {
    val renamed = oneRow.rename("another")

    renamed.tableName shouldBe "another"
    renamed.columns.head.tableName shouldBe "another"
  }

  it should "add an empty column" in {
    val rows2 = oneRow.addColumn(name = "col2", jdbcType = INTEGER)

    rows2.columns(1) shouldBe ColumnDescription(name = "col2", jdbcType = INTEGER, tableName = "atable")
    rows2.data.map(_(1)) should contain theSameElementsAs List("", "")
  }

  it should "add a computed column" in {
    val rows2 =
      oneRow.computeColumn(name = "col2", jdbcType = VARCHAR, row => row("col1") + row("col1"))

    rows2.columns(1) shouldBe ColumnDescription(name = "col2", jdbcType = VARCHAR, tableName = "atable")
    rows2.data.map(_(1)) should contain theSameElementsAs List("data1data1", "data2data2")
  }

  it should "migrate data to a new structure" in {
    val migrated =
      oneRow.migrate(
        newTableName = "table2",
        transformations = Vector(
          CreateColumn(columnName = "col2", jdbcType = VARCHAR, transformData = row => row("col1") + row("col1")),
          CreateColumn(columnName = "col3", jdbcType = INTEGER, transformData = row => row("col1").length.toString)
        )
      )

    migrated shouldBe DataRows(
      tableName = "table2",
      columns = Vector(
        ColumnDescription(name = "col2", jdbcType = VARCHAR, tableName = "table2"),
        ColumnDescription(name = "col3", jdbcType = INTEGER, tableName = "table2")
      ),
      data = Vector(Vector("data1data1", "5"), Vector("data2data2", "5"))
    )
  }

  it should "migrate data to a new structure with complex transformation" in {
    val migrated =
      oneRow.migrate(
        newTableName = "table2",
        transformations = Vector(
          CreateColumn(columnName = "col2", jdbcType = VARCHAR, transformData = row => row("col1") + row("col1")),
          CreateColumn(columnName = "col3", jdbcType = INTEGER, transformData = row => row("col1").length.toString),
          RenameColumn(source = "col1", target = "col4"),
          TransformColumn(columnName = "col1", jdbcType = INTEGER, transformData = _.drop(4))
        )
      )

    migrated shouldBe DataRows(
      tableName = "table2",
      columns = Vector(
        ColumnDescription(name = "col2", jdbcType = VARCHAR, tableName = "table2"),
        ColumnDescription(name = "col3", jdbcType = INTEGER, tableName = "table2"),
        ColumnDescription(name = "col4", jdbcType = VARCHAR, tableName = "table2"),
        ColumnDescription(name = "col1", jdbcType = INTEGER, tableName = "table2")
      ),
      data = Vector(
        Vector("data1data1", "5", "data1", "1"),
        Vector("data2data2", "5", "data2", "2")
      )
    )
  }

  it should "extract data" in {
    manyRows.values("col2") shouldBe Vector("1", "2")
  }

  it should "migrate data to with a Transform/Rename operation" in {
    val migrated =
      oneRow.migrate(
        newTableName = "table2",
        transformations = Vector(
          TransformColumn(oldName = "col1", newName = "colA", jdbcType = INTEGER, transformData = _.drop(4))
        )
      )

    migrated shouldBe DataRows(
      tableName = "table2",
      columns = Vector(
        ColumnDescription(name = "colA", jdbcType = INTEGER, tableName = "table2")
      ),
      data = Vector(Vector("1"), Vector("2"))
    )
  }

  it should "migrate data to a new structure with keeping old columns" in {

    val migrated = manyRows.migrate(newTableName = "table2", transformations = Vector(KeepColumn("col2")))

    migrated shouldBe DataRows(
      tableName = "table2",
      columns = Vector(
        ColumnDescription(name = "col2", jdbcType = VARCHAR, tableName = "table2")
      ),
      data = Vector(Vector("1"), Vector("2"))
    )
  }

  val rowsWith3Columns: DataRows = DataRows(
    tableName = "atable",
    columns = Vector(
      ColumnDescription(name = "col1", jdbcType = VARCHAR, tableName = "atable"),
      ColumnDescription(name = "col2", jdbcType = VARCHAR, tableName = "atable"),
      ColumnDescription(name = "col3", jdbcType = INTEGER, tableName = "atable")
    ),
    data = Vector(Vector("data1", "aa", "1"), Vector("data2", "bb", "2"))
  )

  it should "delete a column" in {
    val deleted = rowsWith3Columns.deleteColumn("col2")
    deleted shouldBe DataRows(
      tableName = "atable",
      columns = Vector(
        ColumnDescription(name = "col1", jdbcType = VARCHAR, tableName = "atable"),
        ColumnDescription(name = "col3", jdbcType = INTEGER, tableName = "atable")
      ),
      data = Vector(Vector("data1", "1"), Vector("data2", "2"))
    )
  }

  it should "rename a column" in {
    rowsWith3Columns.renameColumn("col2", "col4") shouldBe DataRows(
      tableName = "atable",
      columns = Vector(
        ColumnDescription(name = "col1", jdbcType = VARCHAR, tableName = "atable"),
        ColumnDescription(name = "col3", jdbcType = INTEGER, tableName = "atable"),
        ColumnDescription(name = "col4", jdbcType = VARCHAR, tableName = "atable")
      ),
      data = Vector(Vector("data1", "1", "aa"), Vector("data2", "2", "bb"))
    )
  }

  it should "transform a column" in {
    rowsWith3Columns.transformColumn("col2", _.toUpperCase) shouldBe DataRows(
      tableName = "atable",
      columns = Vector(
        ColumnDescription(name = "col1", jdbcType = VARCHAR, tableName = "atable"),
        ColumnDescription(name = "col3", jdbcType = INTEGER, tableName = "atable"),
        ColumnDescription(name = "col2", jdbcType = VARCHAR, tableName = "atable")
      ),
      data = Vector(Vector("data1", "1", "AA"), Vector("data2", "2", "BB"))
    )
  }

  it should "filter columns to keep only some" in {
    val filtered = manyRows.filterColumns("col1")
    filtered shouldBe oneRow
  }

  private val added = manyRows.flatMapRows(row =>
    DayOfWeek.values().toVector.map(day => Map("col1" -> row("col1"), "col2" -> day.toString))
  )

  it should "group Rows by id" in {
    added.groupBy("col1") shouldBe Map(
      "data1" ->
        DataRows(
          tableName = "atable",
          columns = Vector(
            ColumnDescription(name = "col1", jdbcType = VARCHAR, tableName = "atable"),
            ColumnDescription(name = "col2", jdbcType = VARCHAR, tableName = "atable")
          ),
          data = Vector(
            Vector("data1", "MONDAY"),
            Vector("data1", "TUESDAY"),
            Vector("data1", "WEDNESDAY"),
            Vector("data1", "THURSDAY"),
            Vector("data1", "FRIDAY"),
            Vector("data1", "SATURDAY"),
            Vector("data1", "SUNDAY")
          )
        ),
      "data2" ->
        DataRows(
          tableName = "atable",
          columns = Vector(
            ColumnDescription(name = "col1", jdbcType = VARCHAR, tableName = "atable"),
            ColumnDescription(name = "col2", jdbcType = VARCHAR, tableName = "atable")
          ),
          data = Vector(
            Vector("data2", "MONDAY"),
            Vector("data2", "TUESDAY"),
            Vector("data2", "WEDNESDAY"),
            Vector("data2", "THURSDAY"),
            Vector("data2", "FRIDAY"),
            Vector("data2", "SATURDAY"),
            Vector("data2", "SUNDAY")
          )
        )
    )
  }

  it should "provide entries in a collection of Maps" in {
    manyRows.entries shouldBe Vector(
      Map("col1" -> "data1", "col2" -> "1"),
      Map("col1" -> "data2", "col2" -> "2")
    )
  }

  it should "add Rows" in {

    val result: DataRows = DataRows(
      tableName = "atable",
      columns = Vector(
        ColumnDescription(name = "col1", jdbcType = VARCHAR, tableName = "atable"),
        ColumnDescription(name = "col2", jdbcType = VARCHAR, tableName = "atable")
      ),
      data = Vector(
        Vector("data1", "MONDAY"),
        Vector("data1", "TUESDAY"),
        Vector("data1", "WEDNESDAY"),
        Vector("data1", "THURSDAY"),
        Vector("data1", "FRIDAY"),
        Vector("data1", "SATURDAY"),
        Vector("data1", "SUNDAY"),
        Vector("data2", "MONDAY"),
        Vector("data2", "TUESDAY"),
        Vector("data2", "WEDNESDAY"),
        Vector("data2", "THURSDAY"),
        Vector("data2", "FRIDAY"),
        Vector("data2", "SATURDAY"),
        Vector("data2", "SUNDAY")
      )
    )
    added shouldBe result
  }

  it should "lookUp" in {
    val roles: TableFor2[String, Option[String]] = Table(
      ("valueLookUp", "expectedResult"),
      ("aa", Some("data1")),
      ("bb", Some("data2")),
      ("cc", None)
    )

    forAll(roles) {
      case (valueLookUp, expectedResult) =>
        rowsWith3Columns.lookUp("col2", valueLookUp, "col1") shouldBe expectedResult
    }
  }
}
