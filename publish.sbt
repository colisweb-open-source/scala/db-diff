ReleaseSettings.globalReleaseSettings
ReleaseSettings.buildReleaseSettings(
  "library for importing and exporting data and schema as JSON from a database",
  "MIT",
  "http://opensource.org/licenses/MIT",
  "db-diff"
)
ThisBuild / developers := List(
  Developers.michelDaviot,
  Developers.colasMombrun,
  Developers.claireMielcarek,
  Developers.florianDoublet,
  Developers.sallaReznov
)
