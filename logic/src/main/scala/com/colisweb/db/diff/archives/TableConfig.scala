package com.colisweb.db.diff.archives

case class TableConfig(
    tableName: String,
    column: String,
    condition: String,
    batchSize: Int
)
