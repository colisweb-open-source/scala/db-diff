package com.colisweb.db.diff.files

import com.colisweb.db.diff.db.DataRows
import org.slf4j.LoggerFactory
import os.Path
import upickle.default.read

object ReadWrite {
  val logger = LoggerFactory.getLogger(getClass)

  def readFile(path: Path): DataRows = {
    val content = os
      .read(path)
      .replaceAll("\n\\\\n", "\\\\n")
      .replaceAll("\t\\\\t", "\\\\t")

    val rows = read[DataRows](content)
    logger.info(s"read ${rows.data.size} rows from $path")
    rows
  }

  def readFiles(fileName: String): Seq[DataRows] = {
    val path = os.FilePath(fileName).resolveFrom(os.pwd)
    if (os.isFile(path)) Seq(readFile(path))
    else
      for {
        file <- os.list(path)
        if file.toString.endsWith(".json") && !file.toString.endsWith("metadata.json")
      } yield readFile(file)
  }

  def writeFile(service: String, environment: String, rows: DataRows): Unit = {
    val folder = os.pwd / service / environment
    os.makeDir.all(folder)
    val file   = folder / (rows.tableName + ".json")
    os.write.over(file, rows.toJson)
    logger.info(s"exported to $file (${rows.data.size} rows)")
  }

  def writeSchema(service: String, environment: String, schema: String): Unit = {
    val folder = os.pwd / service / environment
    os.makeDir.all(folder)
    val file   = folder / "schema.md"
    os.write.over(file, schema)
    logger.info(s"schema generated to $file")
  }
}
