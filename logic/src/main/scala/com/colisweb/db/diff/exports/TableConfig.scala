package com.colisweb.db.diff.exports

case class TableConfig(
    tableName: String,
    columns: Seq[ColumnConfig] = Nil,
    followKeys: Boolean = false,
    onlyFirstRow: Boolean = false, //TODO: maybe this should be specified on a link instead
    maxRowsCount: Option[Int] = None,
    idColumnName: Option[String] = None,
    idValues: Seq[String] = Nil
) {

  def idColumn: Option[ColumnConfig] =
    for {
      i <- idColumnName
      c <- columns.find(_.name.toLowerCase == i.toLowerCase)
    } yield c

  def skipped: Set[String] =
    (for {
      c <- columns
      if !c.export
    } yield c.name).toSet
}
