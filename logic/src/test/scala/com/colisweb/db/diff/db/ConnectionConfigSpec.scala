package com.colisweb.db.diff.db

import com.github.writethemfirst.Approbation
import org.scalatest.flatspec.FixtureAnyFlatSpec
import org.scalatest.matchers.should.Matchers
import pureconfig.ConfigSource
import pureconfig.generic.auto._

class ConnectionConfigSpec extends FixtureAnyFlatSpec with Matchers with Approbation with DbSetup {

  "ConnectionConfig" should "parse from a MySql config string" in { approver =>
    val config = ConfigSource
      .string("""{
                |  url = "jdbc:p6spy:mysql://localhost:25435/ca_archive"
                |  user = "mysql"
                |  password = "password"
                |  engine = { type = my-sql }
                |}
                |""".stripMargin)
      .loadOrThrow[ConnectionConfig]

    approver.verify(config)
  }

  it should "parse from a PostgreSql config string" in { approver =>
    val config = ConfigSource
      .string("""{
                |  url = "jdbc:postgresql://localhost:5432/postgres?cacheResultsetMetadata=true"
                |  user = "postgres"
                |  password = "password"
                |  engine = { type = postgre-sql }
                |}
                |""".stripMargin)
      .loadOrThrow[ConnectionConfig]

    approver.verify(config)
  }

}
