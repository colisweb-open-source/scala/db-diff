package com.colisweb.db.diff.imports

import pureconfig.ConfigReader
import pureconfig.generic.semiauto.deriveEnumerationReader

object ExistingIdBehaviour {
  implicit val behaviourConvert: ConfigReader[ExistingIdBehaviour] = deriveEnumerationReader[ExistingIdBehaviour]
}

sealed trait ExistingIdBehaviour

case object DeleteAndCreate extends ExistingIdBehaviour
case object Skip            extends ExistingIdBehaviour
case object Create          extends ExistingIdBehaviour
case object Update          extends ExistingIdBehaviour
case object Delete          extends ExistingIdBehaviour
