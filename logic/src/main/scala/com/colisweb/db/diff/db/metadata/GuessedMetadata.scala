package com.colisweb.db.diff.db.metadata

import com.hypertino.inflector.English
import com.colisweb.db.diff.db.metadata.GuessedMetadata._
import com.colisweb.db.diff.exports.{ColumnFilter, TableFilter, TableLink}
import org.slf4j.LoggerFactory

case class GuessedMetadata(
    standardDbMetadata: DatabaseMetadata,
    skippedTables: Set[String] = Set.empty,
    skippedLinks: Set[Link] = Set.empty,
    guessMissingForeignKeys: Boolean = true
) {
  val logger = LoggerFactory.getLogger(getClass)

  import standardDbMetadata._

  lazy val links: Seq[TableLink] =
    (fkLinks ++ polymorphicLinks)
      .flatMap(l => Seq(l, l.reverse))
      .filterNot {
        case TableLink(from, to, _) => from == to || skip(from.table, to.table)
      }

  private lazy val polymorphicLinks: Seq[TableLink] = for {
    table             <- allTables.values.toSeq
    columnPair        <- polymorphicColumns(table)
    targetEntity      <- dbConnection.repository.selectTargets(columnPair)
    targetTableName    = toSnakeCase(targetEntity)
    targetTable       <- allTables.values.filter(_.tableName.matches(s"${targetTableName}s?"))
    targetTableColumn <- targetTable.columns.find(_.name == targetTable.primaryKeyColumns.head)
  } yield TableLink(
    from = TableFilter(
      table.tableName,
      columnPair.idColumn.name,
      List(ColumnFilter(columnPair.typeColumn, List(targetEntity)))
    ),
    to = TableFilter(targetTable.tableName, targetTableColumn.name)
  )

  private def polymorphicColumns(
      tableMetaData: TableMetaData,
      polymorphicIdSuffix: String = "_id",
      polymorphicTypeSuffix: String = "_type"
  ): Seq[PolymorphicColumnPair] =
    for {
      typeColumn <- tableMetaData.columns
      typeName    = typeColumn.name
      if typeName.endsWith(polymorphicTypeSuffix)
      idColumn   <- tableMetaData.columns
      idName      = idColumn.name
      if idColumn.name.endsWith(polymorphicIdSuffix)
      if typeName.dropRight(polymorphicTypeSuffix.length) == idName.dropRight(polymorphicIdSuffix.length)
    } yield PolymorphicColumnPair(tableMetaData.tableName, idColumn, typeColumn)

  for {
    TableLink(TableFilter(fromTable, fromColumn, _), TableFilter(toTable, toColumn, _), _) <- links
  } logger.info(s"$fromTable.$fromColumn -> $toTable.$toColumn")

  private def skip(from: String, to: String): Boolean =
    skippedTables(from) || skippedTables(to) || skippedLinks.exists {
      case Link(f, t) =>
        f.r.matches(from) && t.r.matches(to) && { logger.info(s"Skipping link from $from to $to"); true }
    }

  private lazy val fkLinks: Seq[TableLink] =
    for {
      table      <- allTables.values.toSeq
      tableKey   <- table.primaryKeyColumns //TODO: handle case with composite primary key
      otherTable <- allTables.values
      fkLink     <- linksWith(otherTable, table.tableName, tableKey, allowGuesses = guessMissingForeignKeys)
    } yield fkLink

  private def linksWith(
      table: TableMetaData,
      primaryTable: String,
      idColumn: String,
      allowGuesses: Boolean
  ): Option[TableLink] = {
    def link(foreignColumn: String) =
      TableLink(
        from = TableFilter(table.tableName, foreignColumn),
        to = TableFilter(primaryTable, idColumn)
      )

    table.foreignKeys.find(fk => fk.primaryTable == primaryTable && fk.primaryColumn == idColumn) match {
      case Some(fk)             => Some(link(fk.foreignColumn))
      case None if allowGuesses =>
        val nameRe = s"(?i)${singular(primaryTable)}_?(id|$idColumn)".r
        table.columns
          .find(c => nameRe.matches(c.name))
          .map(c => link(c.name))
      case None                 => None
    }
  }
}

object GuessedMetadata {

  def toSnakeCase(s: String): String =
    s.replaceAll("(?!^)[A-Z]", "_$0").toLowerCase

  def singular(tableName: String): String =
    tableName.split("_").map(w => s"$w|${English.singular(w)}").mkString("(", ")_(", ")")
}
