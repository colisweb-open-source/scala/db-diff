package com.colisweb.db.diff.demo

import com.colisweb.db.diff.db._
import com.colisweb.db.diff.db.metadata.ColumnDescription

import java.sql.JDBCType.{FLOAT, INTEGER, VARCHAR}

//useful for a quick test, like
// Test / runMain com.colisweb.db.diff.SqliteDemo /tmp/sqlitedb
object SqliteDemo extends App {
  val List(file) = args.toList

  val connection: DbConnection = DbConnection(
    ConnectionConfig(
      url = s"jdbc:sqlite:$file",
      user = "",
      password = "",
      SqliteSql
    ),
    service = "test",
    environment = "local"
  )

  val repository: Repository = connection.repository

  repository.withConnection { implicit connection =>
    repository.createAndInsert(
      DataRows(
        "t1",
        Vector(
          ColumnDescription("a", INTEGER, "t1"),
          ColumnDescription("b", VARCHAR, "t1"),
          ColumnDescription("c", FLOAT, "t1")
        ),
        Vector(
          Vector("1", "aa1", "1.0"),
          Vector("2", "a1", "1.5"),
          Vector("3", "aqsda1", "3.0")
        )
      )
    )
    repository.createAndInsert(
      DataRows(
        "t2",
        Vector(
          ColumnDescription("a", INTEGER, "t1"),
          ColumnDescription("b", VARCHAR, "t1"),
          ColumnDescription("c", FLOAT, "t1")
        ),
        Vector(
          Vector("1", "t1", "1.0"),
          Vector("2", "t1", "1.5"),
          Vector("3", "at1", "3.0")
        )
      )
    )
  }

  val rows = repository.sqlSelect("SELECT * from t1 join t2 on t1.a = t2.a ")

  println(rows.aggregate)

}
