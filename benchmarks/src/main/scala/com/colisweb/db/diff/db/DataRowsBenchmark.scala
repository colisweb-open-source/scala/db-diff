package com.colisweb.db.diff.db

import com.colisweb.db.diff.db.metadata.ColumnDescription
import org.openjdk.jmh.annotations._

import java.sql.JDBCType
import java.util.concurrent.TimeUnit
import scala.util.Random

@Warmup(iterations = 5, time = 2000, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 1, time = 2000, timeUnit = TimeUnit.MILLISECONDS)
@Fork(1)
@BenchmarkMode(Array(Mode.AverageTime))
@OutputTimeUnit(TimeUnit.MILLISECONDS)
class DataRowsBenchmark {

  @Benchmark
  def groupBy(state: DataRowsState): Map[String, DataRows] = {
    state.rows.groupBy("col1")
  }
}

@State(Scope.Thread)
class DataRowsState {

  var rows: DataRows = _

  @Setup
  def setup(): Unit = {
    rows = DataRows(
      "table",
      (1 to 24).toVector.map(i => ColumnDescription(s"col$i", JDBCType.VARCHAR, "table")),
      Vector.fill(1000, 24)(Random.nextInt(1000).toString)
    )
  }
}
