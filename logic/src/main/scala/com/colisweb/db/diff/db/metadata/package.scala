package com.colisweb.db.diff.db

/**
  * Represents information about the database schema, like table names, columns names and types,
  * foreign keys ...
  */
package object metadata
