import $file.Configuration
import $file.^.conf.Passwords
import $ivy.`com.colisweb:db-diff_2.13:2.8.1`
import $ivy.`dev.zio:zio-streams_2.13:2.1.16`
import com.colisweb.db.diff.db.DataRows
import mainargs.main
import org.slf4j.{Logger, LoggerFactory}
import zio.ZIO
import zio.stream.{ZSink, ZStream}

import java.sql.JDBCType

val logger: Logger = LoggerFactory.getLogger("person.nationality")

val cityQuery = "SELECT postal_code, country FROM city"

def personQuery(postalCodes: Iterable[String]) =
  s"""
     |SELECT id, birth_postal_code
     |FROM person
     |WHERE birth_postal_code IN (${postalCodes.map(b => s"'$b'").mkString(",")})
     |""".stripMargin

var counter: Int = 0

def applyOnData(chunkSize: Int): Unit = {
  val process = ZStream
    .fromIterator(Configuration.cityRepository.sqlSelect(cityQuery).batch(chunkSize))
    .run(ZSink.foreach(cityDataRows => ZIO.succeed(migrate(cityDataRows))))
  zio.Runtime.default.unsafeRun(process)
}

def migrate(cityDataRows: DataRows): Unit = {

  val postalCodesWithCountry: Map[String, String] =
    cityDataRows.entries.map(data => data("postal_code") -> data("country")).toMap

  val personsToUpdate: DataRows                   =
    Configuration.personDbConnection.repository
      .sqlSelect(personQuery(postalCodesWithCountry.keys))
      .aggregate
      .computeColumn(
        "nationality",
        JDBCType.VARCHAR,
        personRow => postalCodesWithCountry(personRow("birth_postal_code"))
      )

  counter += personsToUpdate.groupBy("id").keys.toList.length
  Configuration.personImportLogic.writeTable(personsToUpdate)
  logger.info(s"number of person updated: $counter")
}

@main
def main(): Unit = {
  applyOnData(5000)
}
