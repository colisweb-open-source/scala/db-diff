package com.colisweb.db.diff.exports

case class ColumnConfig(name: String, replaceWith: Option[String], export: Boolean = true)
