package com.colisweb.db.diff.db

import com.dimafeng.testcontainers.JdbcDatabaseContainer
import org.flywaydb.core.Flyway

import java.io.File

sealed trait DbVendor {
  def flyway: Flyway
  def name: String
  def config: ConnectionConfig
}

case object H2DB extends DbVendor {
  val jdbcUrl = "jdbc:h2:mem:db_diff_test;DB_CLOSE_DELAY=-1;CASE_INSENSITIVE_IDENTIFIERS=TRUE"

  override def flyway: Flyway =
    Flyway
      .configure()
      .locations(
        "classpath:com/colisweb/db/diff/db/migration/shared",
        "classpath:com/colisweb/db/diff/db/migration/postgres"
      )
      .dataSource(jdbcUrl, null, null)
      .load()

  override def name: String = "h2"

  override def config: ConnectionConfig = {
    ConnectionConfig(
      url = jdbcUrl,
      user = null,
      password = null,
      engine = H2Sql
    )
  }
}

case object SQLite extends DbVendor {
  private val tempFile = File.createTempFile("db-diff-test", "sqlite").getCanonicalPath

  val jdbcUrl: String = s"jdbc:sqlite:$tempFile"

  override def flyway: Flyway =
    Flyway
      .configure()
      .locations(
        "classpath:com/colisweb/db/diff/db/migration/shared",
        "classpath:com/colisweb/db/diff/db/migration/sqlite"
      )
      .dataSource(jdbcUrl, null, null)
      .load()

  override def name: String = "sqlite"

  override def config: ConnectionConfig = {
    ConnectionConfig(
      url = jdbcUrl,
      user = null,
      password = null,
      engine = SqliteSql
    )
  }
}

final case class ContainerDb(name: String, container: JdbcDatabaseContainer, engine: DbEngine) extends DbVendor {

  def flyway: Flyway =
    Flyway
      .configure()
      .locations(
        "classpath:com/colisweb/db/diff/db/migration/shared",
        s"classpath:com/colisweb/db/diff/db/migration/$name"
      )
      .dataSource(container.jdbcUrl, container.username, container.password)
      .load()

  def config: ConnectionConfig =
    ConnectionConfig(
      url = container.jdbcUrl,
      user = container.username,
      password = container.password,
      engine = engine
    )
}
