package com.colisweb.db.diff.db

import pprint.PPrinter.BlackWhite

import java.sql.Types._

case class ConnectionConfig(
    url: String,
    user: String,
    password: String,
    engine: DbEngine,
    skippedTables: Set[String] = Set("flyway_schema_history"),
    useCachedMetadata: Boolean = false //Set to false if table schema changed
) {
  override def toString: String = BlackWhite.apply(this.copy(password = "******")).render
}

sealed trait DbEngine {
  def escapeQuote: Char
  def driver: String
  def enumRepresentation: EnumRepresentation
}

final case class EnumRepresentation(jdbcType: Int, typeName: Option[String] = None)

case object MySql extends DbEngine {
  override def escapeQuote: Char = '`'
  override def driver: String    = "com.mysql.cj.jdbc.Driver"

  def enumRepresentation: EnumRepresentation = EnumRepresentation(CHAR, Some("CHAR"))
}

case object PostgreSql extends DbEngine {
  override def escapeQuote: Char = '"'
  override def driver: String    = "org.postgresql.Driver"

  def enumRepresentation: EnumRepresentation = EnumRepresentation(VARCHAR)
}

case object H2Sql extends DbEngine {
  override def escapeQuote: Char = '"'
  override def driver: String    = "org.h2.Driver"

  def enumRepresentation: EnumRepresentation = EnumRepresentation(OTHER, Some("ENUM"))
}

case object SqliteSql extends DbEngine {
  override def escapeQuote: Char = '`'
  override def driver: String    = "org.sqlite.JDBC"

  def enumRepresentation: EnumRepresentation = EnumRepresentation(VARCHAR, Some("TEXT"))
}
