import $file.^.conf.Passwords
import $ivy.`com.colisweb:db-diff_2.13:2.8.1`
import com.colisweb.db.diff.db.{ConnectionConfig, DbConnection, MySql, PostgreSql}
import com.colisweb.db.diff.imports.{ImportConfig, ImportLogic, TableConfig, Update}

val environment = "testing"

val cityConnectionConfig = ConnectionConfig(
  s"jdbc:mysql://city-db-url:city-db-port/city-dab-name",
  "city-db-user",
  Passwords.cityDbPassword,
  MySql
)
val cityRepository = DbConnection(cityConnectionConfig, "city", environment).repository

val personConnectionConfig = ConnectionConfig(
  s"jdbc:mysql://person-db-url:person-db-port/person-dab-name",
  "person-db-user",
  Passwords.personDbPassword,
  PostgreSql
)

val personDbConnection = DbConnection(personConnectionConfig, "person", environment)

val personImportConfig =
  ImportConfig(personConnectionConfig, List(TableConfig("person", List("id"), Update)))
val personImportLogic = ImportLogic(personDbConnection, personImportConfig)