package com.colisweb.db.diff.db

import com.colisweb.db.diff.db.metadata.ColumnDescription

case class RowIterator(rows: Iterator[Vector[String]], columns: Vector[ColumnDescription], tableName: String) {
  def aggregate: DataRows = DataRows(tableName = tableName, columns = columns, data = rows.toVector)

  def batch(size: Int): Iterator[DataRows] =
    rows.grouped(size).map { group => DataRows(tableName = tableName, columns = columns, data = group.toVector) }
}
