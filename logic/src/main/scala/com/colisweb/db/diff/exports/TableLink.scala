package com.colisweb.db.diff.exports

import com.colisweb.db.diff.db.metadata.{ColumnDescription, TableMetaData}

import scala.annotation.tailrec

final case class TableFilter(table: String, idColumn: String, filters: List[ColumnFilter] = Nil) {

  def withValues(column: ColumnDescription, values: Seq[String]): TableFilter =
    copy(filters = ColumnFilter(column, values) :: filters)

  def withId(tableConfig: TableConfig, tableMetaData: TableMetaData): TableFilter =
    (for {
      idColumn   <- tableConfig.idColumn
      if tableConfig.idValues.nonEmpty
      columnDesc <- tableMetaData.column(idColumn.name)
    } yield withValues(columnDesc, tableConfig.idValues)).getOrElse(this)

  def cartesianProduct: Seq[TableFilter] = {
    @tailrec
    def cartesianProductRec(moreFilters: List[ColumnFilter], acc: Seq[TableFilter]): Seq[TableFilter] = {
      moreFilters match {
        case Nil            => acc
        case filter :: tail =>
          val newAcc = filter.values.flatMap { v =>
            if (acc.isEmpty)
              List(copy(filters = List(filter.copy(values = Seq(v)))))
            else
              acc.map(f => f.copy(filters = filter.copy(values = Seq(v)) :: f.filters))
          }
          cartesianProductRec(tail, newAcc)
      }
    }

    if (filters.isEmpty) Seq(this)
    else cartesianProductRec(filters, Nil)
  }

  override def toString: String = s"$table.$idColumn"
}

case class ColumnFilter(column: ColumnDescription, values: Seq[String])

case class TableLink(from: TableFilter, to: TableFilter, reversed: Boolean = false) {
  def reverse: TableLink = TableLink(to, from, true)

}
