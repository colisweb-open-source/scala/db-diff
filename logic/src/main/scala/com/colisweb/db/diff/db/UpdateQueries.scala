package com.colisweb.db.diff.db

import com.colisweb.db.diff.db.metadata._

import java.sql.Connection

trait UpdateQueries { self: RepositoryCommon =>

  def update(
      tableName: String,
      values: Map[ColumnDescription, String],
      where: Vector[(ColumnDescription, String)]
  )(implicit c: Connection): Unit =
    withStatement(s"""UPDATE ${escape(tableName)}
                     | SET ${values.keys.map(d => escape(d.name) + "= ?").mkString(", ")}
                     | WHERE ${whereStatement(where)}
                     | """.stripMargin) { insertStatement =>
      val (columnsName, columnsValues) = where.unzip
      setValues(insertStatement, values.keys.toList ++ columnsName, values.values.toList ++ columnsValues)
      insertStatement.executeUpdate()
    }

  def deleteRow(tableName: String, where: Vector[(ColumnDescription, String)])(implicit c: Connection): Unit =
    withStatement(s"DELETE FROM ${escape(tableName)} WHERE ${whereStatement(where)}") { deleteStatement =>
      where.zipWithIndex.foreach {
        case ((column, value), i) => deleteStatement.setObject(i + 1, value, column.jdbcType)
      }
      deleteStatement.executeUpdate()
    }

}
