# Demo database
(from [postgresDBSamples](https://github.com/morenoh149/postgresDBSamples/tree/master/dellstore2-normal-1.0))
  
    docker run \
      --name pg-diff-demo1 \
      -e POSTGRES_PASSWORD=secret \
      -d \
      -p 54321:5432 \
      --rm \
      -v $(pwd)/src/test/resources/install.sql:/docker-entrypoint-initdb.d/1-init.sql \
      postgres    

    docker run \
      --name pg-diff-demo2 \
      -e POSTGRES_PASSWORD=secret \
      -d \
      -p 54322:5432 \
      --rm \
      -v $(pwd)/src/test/resources/install.sql:/docker-entrypoint-initdb.d/1-init.sql \
      postgres    


# Export from database to json

```plantuml
@startuml
!include  <C4/C4_Context.puml>
!include  <C4/C4_Container.puml>

title from database to json

ContainerDb(db, "database", "SQL")
Container(export, "DbExport", "scala")
Container(file, "files", "json")


Rel_L(export, db, "read", "jdbc")
Rel_R(export, file, "write", "io")

@enduml
```


First argument is the service name `demo`, second is the environment `local1`:

    sbt "runMain com.colisweb.db.diff.DbExport demo local1"

You can also specify several environments

    sbt "runMain com.colisweb.db.diff.DbExport demo local1 local2"
    
Files will be written in `demo/local1/*.json` `demo/local2/*.json`.    
    
# Using DbImport

```plantuml
@startuml
!include  <C4/C4_Context.puml>
!include  <C4/C4_Container.puml>

title from json to database

Container(file, "files", "json")
ContainerDb(db, "database", "SQL")
Container(import, "DbImport", "scala")


Rel_L(import, file, "read", "io")
Rel_R(import, db, "write", "jdbc")

@enduml
```

    sbt "runMain com.colisweb.db.diff.DbImport demo local1 demo/local1/file1.json demo/local1/file2.json"

or to import all files in a folder:

    sbt "runMain com.colisweb.db.diff.DbImport demo local1 demo/local2"

# Transform json

```plantuml
@startuml
!include  <C4/C4_Context.puml>
!include  <C4/C4_Container.puml>

title from json to json

Container(fileI, "input files", "json")
Container(fileO, "output files", "json")
Container(scala, "Transform", "scala")


Rel_L(scala, fileI, "read", "io")
Rel_R(scala, fileO, "write", "io")

@enduml
```
This can be useful for DB migrations or to apply changes.

    sbt "runMain Transform service transformed service_input/environment"

This will take JSON files from `service_input/environment`, 
run the class Transform on them, 
and write the output in folder `service/transformed`

# Generate database Schema with PlantUML

## From scratch

See [DbSchema](logic/src/main/scala/DbSchema.scala) for more details

### With sbt

    sbt "runMain com.colisweb.db.diff.DbSchema -s service -e local -u "jdbc:postgresql://localhost:24441/service_db" -p "secret" -g postgre

### With Coursier

If you don't want to clone this repository or set up a scala project with sbt, ou can simply run this with 
[Coursier](https://get-coursier.io/docs/cli-installation):

    COURSIER_REPOSITORIES="ivy2Local|central|sonatype:releases" \
      cs launch com.colisweb::db-diff:2.6.1 -M DbSchema -- \
        -s parcel \
        -e testing \
        -u "jdbc:postgresql://localhost:24441/parcel_testing" \
        -p "secret" \
        -g postgres

**Note:** 
db-diff seems not to be published to Maven Central at the moment, only on 
[Sonatype](https://oss.sonatype.org/content/repositories/releases/com/colisweb/db-diff_2.13/).
Check on Sonatype for the latest version


## When you already have an `application.conf` file

    sbt "runMain com.colisweb.db.diff.DbSchemaConfig demo local1"

Gives a schema similar to this one:

```plantuml
@startuml

!define table(x) class x << (T,#FFAAAA) >>

hide methods
hide stereotypes

table(cust_hist) {
  customerid int4
  orderid int4
  prod_id int4
}

table(customers) {
  <u>customerid</u> serial
  firstname varchar
  lastname varchar
  address1 varchar
  address2 varchar
  city varchar
  state varchar
  zip int4
  country varchar
  region int2
  email varchar
  phone varchar
  creditcardtype int4
  creditcard varchar
  creditcardexpiration varchar
  username varchar
  password varchar
  age int2
  income int4
  gender varchar
}

table(orderlines) {
  orderlineid int4
  orderid int4
  prod_id int4
  quantity int2
  orderdate date
}

table(orders) {
  <u>orderid</u> serial
  orderdate date
  customerid int4
  netamount numeric
  tax numeric
  totalamount numeric
}

cust_hist::customerid --> customers::customerid
orderlines::orderid --> orders::orderid
orders::customerid --> customers::customerid


@enduml
```

## Local tests

To run only h2 tests : `testOnly -- -z "h2"`

# Examples

The module [demo](demo) contains usage examples. 
Both package ammonite and scala contain the same example, but ammonite files are scripts set up to be run  with ammonite, whereas package scala contains a main method which can be run with sbt. 

## Ammonite
The scripts are organized to be run on kubernetes with a custom . 
```
run_job_k8s -e testing -p person-nationality -c demo/src/main/scala/com/colisweb/db/diff/demo/ammonite/conf/Passwords.sc -s demo/src/main/scala/com/colisweb/db/diff/demo/ammonite/code/Migration.sc -f demo/src/main/scala/com/colisweb/db/diff/demo/ammonite/code/
```
