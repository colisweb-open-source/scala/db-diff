package com.colisweb.db.diff.db

import com.colisweb.db.diff.db.metadata.{DatabaseMetadata, TableMetaData}
import org.slf4j.LoggerFactory

import java.sql.{Connection, DriverManager}
import java.util.Properties
import scala.jdk.CollectionConverters._

/**
  * Wrapper / builder for a [[java.sql.Connection]] and a [[RepositoryCommon]]
  */
case class DbConnection(connectionConfig: ConnectionConfig, service: String, environment: String) {
  val logger   = LoggerFactory.getLogger(getClass)
  val metadata = DatabaseMetadata(this)

  // Only use perfRepository if you care more about performance than about integrity (FK, unique keys)
  lazy val repository: Repository     = new Repository(connection, connectionConfig)
  lazy val perfRepository: Repository = new Repository(perfConnection, connectionConfig)

  def tableMetadata(tableName: String): TableMetaData = metadata.allTables(tableName)

  def connection(): Connection = {
    import connectionConfig._
    registerDriver(engine.driver)
    logger.debug(s"Connecting to database $url as $user")
    val c = DriverManager.getConnection(url, jdbcProperties(user, password))
    logger.debug("Connected !")
    c
  }

  def perfConnection(): Connection = {
    import connectionConfig._
    registerDriver(engine.driver)
    logger.debug(s"Connecting to database $url as $user (skipping unicity and FK checks)")
    val properties = jdbcProperties(
      user,
      password,
      Map(
        "sessionVariables"         -> "unique_checks=0,foreign_key_checks=0",
        "rewriteBatchedStatements" -> "true",
        "allowLoadLocalInfile"     -> "true",
        "useConfigs"               -> "maxPerformance"
      )
    )
    val c          = DriverManager.getConnection(url, properties)
    logger.debug("Connected !")
    c
  }

  private def jdbcProperties(user: String, password: String, extra: Map[String, String] = Map.empty) = {
    val properties = new Properties()
    val all        = Map("user" -> user, "password" -> password, "cacheResultsetMetadata" -> "true") ++ extra
    properties.putAll(all.filter(_._2 != null).asJava)
    properties
  }

  private def registerDriver(driver: String) = {
    Class.forName("com.p6spy.engine.spy.P6SpyDriver").getDeclaredConstructor().newInstance()
    Class.forName(driver).getDeclaredConstructor().newInstance()
  }
}
