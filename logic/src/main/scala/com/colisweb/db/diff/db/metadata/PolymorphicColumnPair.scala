package com.colisweb.db.diff.db.metadata

/**
  * A polymorphic link is a concept sometimes used in Ruby on Rails applications.
  * It is similar to a foreign key, but the foreign table has an extra column to indicate
  * which primary table is concerned for a given row.
  *
  * Ex:
  *
  *   - primary tables `Buyer` and `Store`
  *   - polymorphic foreign table `Location`, with columns
  *     - `locationable_id` (matches with `buyer.id` or `store.id`)
  *     - `location_type` (either `buyer` or `store`)
  */
case class PolymorphicColumnPair(table: String, idColumn: ColumnDescription, typeColumn: ColumnDescription)
