CREATE TABLE customers (
    customerid integer NOT NULL PRIMARY KEY,
    firstname character varying(50) NOT NULL,
    lastname character varying(50) NOT NULL,
    address1 character varying(50) NOT NULL,
    address2 character varying(50),
    city character varying(50) NOT NULL,
    zip integer,
    country character varying(50) NOT NULL,
    email character varying(50),
    phone character varying(50),
    username character varying(50) NOT NULL,
    password character varying(50) NOT NULL,
    created_at timestamp not null
);

insert into customers (customerid, firstname, lastname, address1, city, country, username, password, created_at) values
(1, 'Jean', 'Valjean', '1 rue du Tertre', 'Lyon', 'France', 'jeanvaljean80', 'ssdf45sdf54sd', '2020-06-22 16:11:53'),
(2, 'Jean', 'Dupont', '1 rue du Chat', 'Lille', 'France', 'jeand', 'qsdqsdqsd', '2020-06-22 16:11:53')
;

CREATE TABLE orders (
    orderid integer NOT NULL PRIMARY KEY,
    orderdate date NOT NULL,
    customerid integer,
    totalamount numeric(12,2) NOT NULL
);



insert into orders (orderid, orderdate, customerid, totalamount) values
(1, '2020-01-01', 1, 33.99),
(2, '2020-01-02', 2, 32.99),
(3, '2020-01-03', 2, 34.99);

CREATE TABLE products (
    productid integer NOT NULL PRIMARY KEY,
    productname character varying(50)
);
