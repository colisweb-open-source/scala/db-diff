import DependenciesScopesHandler.*
import Dependencies.*
import PublishSettings.noPublishSettings

resolvers ++= Resolver.sonatypeOssRepos("snapshots")

inThisBuild {
  List(
    PublishSettings.localCacheSettings,
    scalafmtOnCompile := CISettings.notOnCI,
    scalafmtCheck := CISettings.notOnCI,
    scalafmtSbtCheck := CISettings.notOnCI,
    scalaVersion := "2.13.16",
    organization := "com.colisweb",
    Test / fork := true,
    Test / testForkedParallel := false
  )
}
Global / onChangedBuildSource := ReloadOnSourceChanges

lazy val dbDiff = Project(id = "db-diff", base = file("."))
  .settings(name := "db-diff")
  .settings(noPublishSettings)
  .aggregate(logic, demo, benchmarks)

lazy val logic = project
  .settings(moduleName := "db-diff")
  .settings(
    libraryDependencies ++= compileDependencies(
      inflector,
      logbackClassic,
      mainargs,
      mysqlConnector,
      osLib,
      p6spy,
      postgresql,
      pprint,
      pureconfig,
      sqliteJdbc,
      upickle
    )
  )
  .settings(
    libraryDependencies ++= testDependencies(
      approvalsScala,
      flywayCore,
      h2,
      testContainerJdbc,
      testContainerMysql,
      testContainerPostgresql,
      testcontainers,
      testcontainersScalaMysql,
      testcontainersScalaPostgresql,
      testcontainersScalaScalatest
    )
  )

lazy val benchmarks = project
  .enablePlugins(JmhPlugin)
  .dependsOn(logic)
  .settings(noPublishSettings)

lazy val demo = project
  .dependsOn(logic)
  .settings(noPublishSettings)
  .settings(libraryDependencies ++= compileDependencies(zioSreams))

// uncomment to auto-approve, then sbt test, then commit
//ThisBuild / Test / fork := true
//ThisBuild / Test / javaOptions += "-DAUTO_APPROVE=true"
