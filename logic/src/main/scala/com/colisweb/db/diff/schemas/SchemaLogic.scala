package com.colisweb.db.diff.schemas

import com.colisweb.db.diff.db.DbConnection
import com.colisweb.db.diff.db.metadata.{ColumnDescription, ForeignKey, GuessedMetadata, TableMetaData}
import com.colisweb.db.diff.exports.TableLink

final case class SchemaLogic(
    dbConnection: DbConnection,
    schemaConfig: SchemaConfig
) {

  private val tables: Iterable[TableMetaData] = {
    val includedNames =
      if (schemaConfig.tables.nonEmpty) schemaConfig.tables
      else dbConnection.metadata.allTables.keys
    val keptNames     = includedNames.filterNot(schemaConfig.connection.skippedTables).toSet
    dbConnection.metadata.allTables.collect {
      case (name, tableMetaData) if keptNames(name) => tableMetaData
    }
  }

  private val tableNames: Set[String] = tables.map(_.tableName).toSet

  def generateSchema: String = {
    val metadata =
      GuessedMetadata(standardDbMetadata = dbConnection.metadata, skippedTables = schemaConfig.connection.skippedTables)

    s"""
       |$header
       |
       |${tables.map(tableSchema).mkString("\n")}
       |
       |${tables.map(_.tableName).flatMap(foreignKeys(metadata)).mkString("\n")}
       |
       |$footer
       |""".stripMargin
  }

  private def tableSchema(tableMetaData: TableMetaData): String =
    s"""
       |table(${tableMetaData.tableName}) {
       |  ${tableMetaData.columns.map(tableColumn(tableMetaData)).mkString("\n  ")}
       |}
       |""".stripMargin

  private def tableColumn(tableMetaData: TableMetaData)(column: ColumnDescription): String = {
    val columnName =
      if (tableMetaData.primaryKeyColumns.contains(column.name)) s"<u>${column.name}</u>" else column.name
    s"$columnName ${column.typeName}"
  }

  private def foreignKeys(databaseMetadata: GuessedMetadata)(tableName: String): Seq[String] = {
    val realForeignKeys = databaseMetadata.standardDbMetadata.allTables(tableName).foreignKeys.collect {
      case ForeignKey(name, primaryColumn, primaryTable, foreignColumn) if tableNames(primaryTable) =>
        FkLink(
          fromTable = tableName,
          fromColumn = foreignColumn,
          toTable = primaryTable,
          toColumn = primaryColumn,
          name = name
        )
    }
    val guessedLinks    = databaseMetadata.links.collect {
      case TableLink(from, to, reversed) if !reversed && from.table == tableName && tableNames(to.table) =>
        FkLink(
          fromTable = from.table,
          fromColumn = from.idColumn,
          toTable = to.table,
          toColumn = to.idColumn,
          name = "guessed"
        )

    }
    (realForeignKeys ++ guessedLinks)
      .distinctBy(_.productIterator.toList.init) // ignore the name
      .map(_.toString)
  }

  private val header =
    """
      |```plantuml
      |@startuml
      |
      |!define table(x) class x << (T,#FFAAAA) >>
      |
      |hide methods
      |hide stereotypes
      |
      |left to right direction
      |
      |""".stripMargin

  private val footer =
    """
      |@enduml
      |```
      |""".stripMargin
}

case class FkLink(fromTable: String, fromColumn: String, toTable: String, toColumn: String, name: String) {

  override def toString: String =
    s"$fromTable::$fromColumn --> $toTable::$toColumn : $name"
}
