package com.colisweb.db.diff.db

import com.dimafeng.testcontainers._
import org.scalatest.{BeforeAndAfterAll, Suite}
import org.testcontainers.utility.DockerImageName

trait DbSetup extends ForAllTestContainer with BeforeAndAfterAll { self: Suite =>
  override def container: Container = MultipleContainers(mysqlContainer, postgresContainer)

  private val mysqlContainer = MySQLContainer(
    databaseName = "mysql-test-db",
    username = "scala",
    password = "scala",
    mysqlImageVersion = DockerImageName.parse("mysql:8.0.27") // default is 5.7.34 which does not support TLS 1.2
  )

  private val postgresContainer =
    PostgreSQLContainer(
      databaseName = "pg-test-db",
      username = "scala",
      password = "scala",
      dockerImageNameOverride = DockerImageName.parse("postgres:10.18")
    ) // default is 9.6 no longer supported by Flyway Community Edition

  val myqslVendor: DbVendor    = ContainerDb("mysql", mysqlContainer, MySql)
  val postgresVendor: DbVendor = ContainerDb("postgres", postgresContainer, PostgreSql)

  val dbs: Seq[DbVendor] =
    Seq(H2DB, SQLite, myqslVendor, postgresVendor)

}
