package com.colisweb.db.diff

import com.colisweb.db.diff.db.DbConnection
import com.colisweb.db.diff.db.metadata.DatabaseMetadata
import com.colisweb.db.diff.exports.{ExportConfig, ExportLogic}
import com.colisweb.db.diff.files.ReadWrite

object DbExport extends App {
  val service :: environments = args.toList

  for {
    environment <- environments
    exportConfig = ExportConfig(service, environment)
    dbConnection = DbConnection(exportConfig.connection, service, environment)
    table       <- exportedTables(dbConnection.metadata, exportConfig)
    logic        = ExportLogic(dbConnection, exportConfig).ExportLogicForTable(table)
    rows        <- logic.readTableRecursively()
  } {
    ReadWrite.writeFile(service, environment, rows)
    //TODO: close connection
  }

  def exportedTables(metadata: DatabaseMetadata, exportConfig: ExportConfig): Seq[String] = {
    if (exportConfig.exportedTables.nonEmpty) exportConfig.exportedTables
    else metadata.allTableNames.filterNot(exportConfig.connection.skippedTables)
  }

}
