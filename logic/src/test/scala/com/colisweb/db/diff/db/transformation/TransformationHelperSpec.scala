package com.colisweb.db.diff.db.transformation

import com.colisweb.db.diff.db.transformation.TransformationHelper._
import org.scalatest.matchers.should.Matchers._
import org.scalatest.prop.TableDrivenPropertyChecks._
import org.scalatest.prop.Tables.Table
import org.scalatest.wordspec.AnyWordSpec

class TransformationHelperSpec extends AnyWordSpec {

  "defined or none" in {
    val tests = Table(
      ("string", "result"),
      ("   \t     ", None),
      ("null", None),
      ("", None),
      ("   truc   ", Some("   truc   "))
    )
    forAll(tests) { case (string, expectedResult) => string.definedOrNone shouldBe expectedResult }
  }

  "clean empty string" in {
    val tests = Table(
      ("string", "result"),
      ("        \t", "null"),
      ("null", "null"),
      ("", "null"),
      ("   truc   ", "   truc   ")
    )
    forAll(tests) { case (string, expectedResult) => string.cleanEmptyString shouldBe expectedResult }
  }

  "format or null" in {
    def formatFunction(value: String) = {
      if (value.isBlank) Left("Value is blank")
      else Right(value.trim.toUpperCase)
    }

    val tests = Table(
      ("string", "result"),
      ("    \t    ", "null"),
      ("", "null"),
      ("null", "NULL"),
      ("   truc   ", "TRUC")
    )
    forAll(tests) { case (string, expectedResult) => string.formatOrNull(formatFunction) shouldBe expectedResult }
  }

  "option to db option" in {
    val tests = Table(
      ("string", "result"),
      (Some("x"), "x"),
      (None, "null")
    )
    forAll(tests) { case (string, expectedResult) => string.toDbOption shouldBe expectedResult }
  }

}
