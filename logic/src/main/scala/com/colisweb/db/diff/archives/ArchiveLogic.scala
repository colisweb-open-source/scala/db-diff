package com.colisweb.db.diff.archives

import com.colisweb.db.diff.db.DbConnection
import org.slf4j.LoggerFactory

final case class ArchiveLogic(
    importDbConnection: DbConnection,
    exportDbConnection: DbConnection,
    archiveConfig: ArchiveConfig
) {
  val logger = LoggerFactory.getLogger(getClass)

  def export(tableConfig: TableConfig): Unit = {
    val batchExport = exportDbConnection.repository
      .selectAll(tableConfig.tableName, tableConfig.column + tableConfig.condition)
      .batch(tableConfig.batchSize)

    importDbConnection.perfRepository.withConnection { implicit importConnection =>
      batchExport
        .foreach { rows =>
          val (writeTime, _) = timeMs(importDbConnection.perfRepository.loadBatchMySql(rows))
          logger.info(s"${rows.data.size} rows inserted in $writeTime ms")
        }
    }
  }

  def timeMs[T](code: => T): (Long, T) = {
    val start = System.currentTimeMillis()
    val res   = code
    val time  = System.currentTimeMillis() - start
    (time, res)
  }
}
