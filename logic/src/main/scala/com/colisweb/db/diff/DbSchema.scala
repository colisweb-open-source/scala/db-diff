package com.colisweb.db.diff

import com.colisweb.db.diff.db._
import com.colisweb.db.diff.files.ReadWrite
import com.colisweb.db.diff.schemas.{SchemaConfig, SchemaLogic}
import mainargs.{ParserForMethods, TokensReader, arg, main}

//to use as a standalone main
object DbSchema {
  val supportedDbEngines: Seq[DbEngine] = Seq(SqliteSql, MySql, PostgreSql, H2Sql)

  // example: runMain com.colisweb.db.diff.DbSchema -s parcel -e testing -u "jdbc:postgresql://localhost:24441/parcel_testing" -p "secret" -g "postgre"
  @main
  def run(
      @arg(short = 's', doc = "Service name") service: String,
      @arg(short = 'u', doc = "jdbc URL such as jdbc:postgresql://localhost:24441/parcel_testing") url: String,
      @arg(short = 'l', doc = "Option DB login, defaults to service_env like parcel_testing") login: Option[String],
      @arg(short = 'p', doc = "DB password") password: String,
      @arg(short = 'g', doc = s"DB Engine such as postgres, MySQL (case ignored)") engine: DbEngine,
      @arg(short = 'e', doc = "environment : testing, staging, production") environment: String
  ): Unit = {
    val user         = login.getOrElse(s"${service}_$environment")
    val config       = ConnectionConfig(url, user, password, engine)
    val dbConnection = DbConnection(config, service, environment)
    val logic        = SchemaLogic(dbConnection, SchemaConfig(connection = config))
    ReadWrite.writeSchema(service, environment, logic.generateSchema)
    //TODO: close connection
  }

  def main(args: Array[String]): Unit = ParserForMethods(this).runOrThrow(args)

  implicit val dbEngineReader: TokensReader[DbEngine] = new TokensReader.Simple[DbEngine] {

    override def shortName: String = "DB Engine"

    override def read(strings: Seq[String]): Either[String, DbEngine] = {
      val first = strings.head
      supportedDbEngines
        .collectFirst {
          case engine if engine.getClass.getSimpleName.toLowerCase startsWith first.toLowerCase => engine
        }
        .toRight(
          s"Invalid DbEngine $strings, valid values are ${supportedDbEngines.map(_.getClass.getSimpleName).mkString(",")}"
        )
    }
  }
}

// to use with application.conf
object DbSchemaConfig {

  @main
  def run(
      @arg(short = 's', doc = "Service name, should have its entry in application.conf")
      service: String,
      @arg(short = 'e', doc = "Environment, should be testing, staging or production")
      environment: String
  ): Unit = {
    val schemaConfig = SchemaConfig(service, environment)
    val dbConnection = DbConnection(schemaConfig.connection, service, environment)
    val logic        = SchemaLogic(dbConnection, schemaConfig)
    ReadWrite.writeSchema(service, environment, logic.generateSchema)
    //TODO: close connection
  }

  def main(args: Array[String]): Unit = ParserForMethods(this).runOrThrow(args)

}
