package com.colisweb.db.diff.exports

import com.colisweb.db.diff.db.ConnectionConfig
import com.colisweb.db.diff.db.metadata.Link
import pureconfig._
import pureconfig.generic.auto._

/**
  * Mapped to [[https://github.com/lightbend/config Hocon]] configuration in src/main/resources
  *
  * Default behaviour is to export every table.
  *
  * If you select only a few tables, you can ask to follow foreign keys and to
  * guess missing foreign keys based on table name / column names.
  * Ex: table `customer` with column `order_id` and table `order` with column `id`.
  *
  * This defines how you want to export data and do not necessarily
  * match exactly the database schema (which is gathers in [[com.colisweb.db.diff.db.metadata]].
  */
case class ExportConfig(
    connection: ConnectionConfig,
    tables: List[TableConfig] = Nil,
    exportedTables: Seq[String] = Nil,
    skippedColumns: Set[String] = Set.empty,
    skippedLinks: Set[Link] = Set.empty,
    guessMissingForeignKeys: Boolean = true
) {
  def tableNames: Seq[String] = tables.map(_.tableName.toLowerCase)

  def tableConfig(tableName: String): Option[TableConfig] =
    tables.find(_.tableName.toLowerCase == tableName.toLowerCase)

}

object ExportConfig {

  def apply(service: String, environment: String): ExportConfig =
    ConfigSource.default.at(service).at(environment).at("export").loadOrThrow[ExportConfig]
}
