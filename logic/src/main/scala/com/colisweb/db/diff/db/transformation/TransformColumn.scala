package com.colisweb.db.diff.db.transformation

import com.colisweb.db.diff.db.DataRows
import com.colisweb.db.diff.db.metadata.ColumnDescription

import java.sql.JDBCType

final case class TransformColumn(
    oldName: String,
    newName: String,
    jdbcType: Option[JDBCType],
    typeName: Option[String],
    transformData: String => String
) extends MigrateColumn {

  override def description(rows: DataRows): ColumnDescription = {
    val oldColumn = rows.column(oldName)
    oldColumn.copy(
      name = newName,
      typeName = typeName.getOrElse(oldColumn.typeName),
      jdbcType = jdbcType.fold(oldColumn.jdbcType)(_.getVendorTypeNumber),
      tableName = rows.column(oldName).tableName
    )
  }

  override def transform(data: Map[String, String]): String = transformData(data(oldName))

  override def oldColumnName: Option[String] = Some(oldName)

}

object TransformColumn {

  def apply(
      columnName: String,
      transformData: String => String
  ): TransformColumn =
    TransformColumn(
      oldName = columnName,
      newName = columnName,
      jdbcType = None,
      typeName = None,
      transformData = transformData
    )

  def apply(
      columnName: String,
      jdbcType: JDBCType,
      typeName: String,
      transformData: String => String
  ): TransformColumn =
    TransformColumn(
      oldName = columnName,
      newName = columnName,
      jdbcType = Some(jdbcType),
      typeName = Some(typeName),
      transformData = transformData
    )

  def apply(columnName: String, jdbcType: JDBCType, transformData: String => String): TransformColumn =
    TransformColumn(
      oldName = columnName,
      newName = columnName,
      jdbcType = Some(jdbcType),
      typeName = Some(jdbcType.getName),
      transformData = transformData
    )

  def apply(oldName: String, newName: String, jdbcType: JDBCType, transformData: String => String): TransformColumn =
    TransformColumn(
      oldName = oldName,
      newName = newName,
      jdbcType = Some(jdbcType),
      typeName = Some(jdbcType.getName),
      transformData = transformData
    )

}
