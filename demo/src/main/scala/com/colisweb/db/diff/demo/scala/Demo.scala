import com.colisweb.db.diff.db.DataRows
import com.colisweb.db.diff.demo.scala.Configuration._
import mainargs.main
import org.slf4j.{Logger, LoggerFactory}
import zio.{Unsafe, ZIO}
import zio.stream.{ZSink, ZStream}

import java.sql.JDBCType

object Demo {

  val logger: Logger = LoggerFactory.getLogger("person.nationality")

  val cityQuery = "SELECT postal_code, country FROM city"

  def personQuery(postalCodes: Iterable[String]) =
    s"""
       |SELECT id, birth_postal_code
       |FROM person
       |WHERE birth_postal_code IN (${postalCodes.map(b => s"'$b'").mkString(",")})
       |""".stripMargin

  var counter: Int = 0

  def applyOnData(chunkSize: Int): Unit = {
    val process = ZStream
      .fromIterator(cityRepository.sqlSelect(cityQuery).batch(chunkSize))
      .run(ZSink.foreach(cityDataRows => ZIO.succeed(migrate(cityDataRows))))
    Unsafe.unsafe { implicit unsafe =>
      zio.Runtime.default.unsafe.run(process).getOrThrowFiberFailure()
    }
  }

  def migrate(cityDataRows: DataRows): Unit = {

    val postalCodesWithCountry: Map[String, String] =
      cityDataRows.entries.map(data => data("postal_code") -> data("country")).toMap

    val personsToUpdate: DataRows                   =
      personDbConnection.repository
        .sqlSelect(personQuery(postalCodesWithCountry.keys))
        .aggregate
        .computeColumn(
          "nationality",
          JDBCType.VARCHAR,
          personRow => postalCodesWithCountry(personRow("birth_postal_code"))
        )

    counter += personsToUpdate.groupBy("id").keys.toList.length
    personImportLogic.writeTable(personsToUpdate)
    logger.info(s"number of person updated: $counter")
  }

  @main def main(args: Array[String]): Unit = {
    applyOnData(5000)
  }
}
