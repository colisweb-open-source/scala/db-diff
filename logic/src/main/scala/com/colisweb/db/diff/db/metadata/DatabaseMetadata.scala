package com.colisweb.db.diff.db.metadata

import com.colisweb.db.diff.db.SelectQueries._
import com.colisweb.db.diff.db._
import org.slf4j.LoggerFactory

import java.lang.String.CASE_INSENSITIVE_ORDER
import java.sql.DatabaseMetaData
import scala.collection.SortedMap

/**
  * Holds metadata (tables, column types, primary and foreign keys) for a [[DbConnection]]
  */
case class DatabaseMetadata(dbConnection: DbConnection) {
  val logger = LoggerFactory.getLogger(getClass)

  import dbConnection._

  lazy val allTables: SortedMap[String, TableMetaData] =
    SortedMap(
      (if (connectionConfig.useCachedMetadata)
         TableMetaData.readMetaData(service, environment).getOrElse(tablesMetaData)
       else
         tablesMetaData).toSeq: _*
    )(Ordering.comparatorToOrdering(CASE_INSENSITIVE_ORDER))

  // This is used to compute the previous value `allTables`
  lazy val allTableNames: Seq[String] =
    resultSetAsRows(metaData.getTables(connection.getCatalog, null, null, Array("TABLE")))
      .map("table_name")
      .flatten
      .map(_.toLowerCase)

  private lazy val metaData: DatabaseMetaData = connection.getMetaData

  private lazy val tablesMetaData: Map[String, TableMetaData] = {
    logger.info("gathering database metadata for ")
    val data = for {
      table <- allTableNames
      if !connectionConfig.skippedTables(table)
    } yield {
      logger.info(s"$table ")
      table -> TableMetaData(
        table,
        primaryKeyColumns(table),
        foreignKeys(table),
        allColumns.filter(_.tableName.toUpperCase == table.toUpperCase)
      )

    }
    logger.info("Done reading metadata")
    val md   = data.toMap
    TableMetaData.writeMetaData(service, environment, md)
    md
  }

  //MySQL does not allow to get all keys without specifying the table
  private def primaryKeyColumns(table: String): Vector[String] =
    resultSetAsRows(metaData.getPrimaryKeys(connection.getCatalog, null, table))
      .map("column_name")
      .flatten

  private def foreignKeys(table: String): Seq[ForeignKey] =
    resultSetAsRows(metaData.getImportedKeys(connection.getCatalog, null, table))
      .map("pkcolumn_name", "pktable_name", "fkcolumn_name", "fk_name")
      .collect {
        case Vector(primaryColumn, primaryTable, foreignColumn, name) =>
          ForeignKey(name, primaryColumn, primaryTable, foreignColumn)
      }
      .distinct

  private lazy val allColumns: Seq[ColumnDescription] =
    resultSetAsRows(metaData.getColumns(connection.getCatalog, connection.getSchema, null, null))
      .map("COLUMN_NAME", "TYPE_NAME", "DATA_TYPE", "TABLE_NAME")
      .map(ColumnDescription.apply)
      .distinct

}
