package com.colisweb.db.diff.db

import com.colisweb.db.diff.db.metadata._

import java.io.ByteArrayInputStream
import java.sql.Connection

trait InsertQueries { self: RepositoryCommon =>

  def createAndInsertWithNewConnection(rows: DataRows): Unit = withConnection(implicit c => createAndInsert(rows))

  def createAndInsert(rows: DataRows)(implicit c: Connection): Unit = {
    createTable(rows.tableName, rows.columns)
    insertBatch(rows)
  }

  def createTable(tableName: String, columns: Seq[ColumnDescription])(implicit c: Connection): Unit =
    executeQuery(
      columns
        .map(c => s"${escape(c.name)} ${c.typeName}")
        .mkString(start = s"CREATE TABLE IF NOT EXISTS ${escape(tableName)} (", sep = ",", end = ")")
    )

  def insertBatch(dataRows: DataRows)(implicit c: Connection): Unit = {
    val autoCommit = c.getAutoCommit
    c.setAutoCommit(false)

    val insertStatement = c.prepareStatement(
      s"""INSERT INTO ${escape(dataRows.tableName)}
         | ${dataRows.columns.map(c => escape(c.name)).mkString("(", ", ", ")")}
         | VALUES ( ${Seq.fill(dataRows.columns.length)("?").mkString(", ")} ) """.stripMargin
    )

    dataRows.data.foreach { values =>
      setValues(insertStatement, dataRows.columns, values)
      insertStatement.addBatch()
    }

    insertStatement.executeBatch()
    c.commit()
    c.setAutoCommit(autoCommit)
  }

  //TODO: implement the load batch feature also for other DBMS like PostgreSQL
// https://medium.com/@jerolba/persisting-fast-in-database-load-data-and-copy-caf645a62909
  def loadBatchMySql(dataRows: DataRows)(implicit c: Connection): Unit = {
    val loadStatement = c.unwrap(classOf[com.mysql.cj.jdbc.JdbcConnection]).createStatement()
    val loadData      =
      s"""LOAD DATA LOCAL INFILE '' INTO TABLE ${escape(dataRows.tableName)}
         | CHARACTER SET UTF8 FIELDS TERMINATED BY '\t' ENCLOSED BY ''
         | ESCAPED BY '\\\\' LINES TERMINATED BY '\n'  STARTING BY ''
         | ${dataRows.columns.map(c => escape(c.name)).mkString("(", ", ", ")")} """.stripMargin

    val s = dataRows.data.map(_.mkString("\t")).mkString("\n")

    loadStatement
      .asInstanceOf[com.mysql.cj.jdbc.JdbcStatement]
      .setLocalInfileInputStream(new ByteArrayInputStream(s.getBytes))
    loadStatement.execute(loadData)

  }

  def insertRow(values: Vector[String], dataRows: DataRows)(implicit c: Connection): Unit =
    insertRow(values, dataRows.tableName, dataRows.columns)

  def insertRow(values: Vector[String], tableName: String, columns: Vector[ColumnDescription])(implicit
      c: Connection
  ): Unit =
    withStatement(s"""INSERT INTO ${escape(tableName)}
                     | ${columns.map(c => escape(c.name)).mkString("(", ", ", ")")}
                     | VALUES ( ${Seq.fill(values.length)("?").mkString(", ")} ) """.stripMargin) { s =>
      setValues(s, columns, values)
      s.executeUpdate()
    }

}
