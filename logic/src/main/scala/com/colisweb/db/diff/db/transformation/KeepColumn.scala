package com.colisweb.db.diff.db.transformation

import com.colisweb.db.diff.db.DataRows
import com.colisweb.db.diff.db.metadata.ColumnDescription

final case class KeepColumn(columnName: String) extends MigrateColumn {

  override def description(rows: DataRows): ColumnDescription = rows.column(columnName)

  override def transform(data: Map[String, String]): String = data(columnName)

  override def oldColumnName: Option[String] = Some(columnName)

}
