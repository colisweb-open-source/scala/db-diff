package com.colisweb.db.diff.imports

import com.colisweb.db.diff.db.ConnectionConfig
import pureconfig._
import pureconfig.generic.auto._

/**
  * Mapped to [[https://github.com/lightbend/config Hocon]] configuration in src/main/resources
  *
  * Default behaviour is to import all data provided as JSON.
  */
case class ImportConfig(
    connection: ConnectionConfig,
    tables: List[TableConfig] = Nil
) {
  def tableNames: Seq[String] = tables.map(_.tableName.toLowerCase)

  def tableConfig(tableName: String): Option[TableConfig] =
    tables.find(_.tableName.toLowerCase == tableName.toLowerCase)

}

object ImportConfig {

  def apply(service: String, environment: String): ImportConfig =
    ConfigSource.default.at(service).at(environment).at("import").loadOrThrow[ImportConfig]
}
