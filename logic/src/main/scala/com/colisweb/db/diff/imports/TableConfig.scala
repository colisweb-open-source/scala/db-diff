package com.colisweb.db.diff.imports

case class TableConfig(
    tableName: String,
    idColumns: List[String] = Nil,
    idBehaviour: ExistingIdBehaviour = Create
)
