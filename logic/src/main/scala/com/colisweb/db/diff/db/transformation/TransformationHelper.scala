package com.colisweb.db.diff.db.transformation

object TransformationHelper {

  implicit class StringUtil(value: String) {

    val definedOrNone: Option[String] = if (value.isBlank || value == "null") None else Some(value)

    def formatOrNull(formattingMethod: String => Either[String, String]): String =
      formattingMethod(value).toOption.toDbOption

    def cleanEmptyString: String = definedOrNone.toDbOption

  }

  implicit class StringOptionUtils(value: Option[String]) {

    def toDbOption: String = value.getOrElse("null")

  }
}
