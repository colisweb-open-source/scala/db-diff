package com.colisweb.db.diff.db.metadata

case class ForeignKey(name: String, primaryColumn: String, primaryTable: String, foreignColumn: String)

object ForeignKey {
  import upickle.default._

  implicit val readWriter: ReadWriter[ForeignKey] = macroRW[ForeignKey]
}
