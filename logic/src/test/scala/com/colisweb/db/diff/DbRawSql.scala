package com.colisweb.db.diff

import com.colisweb.db.diff.db.{ConnectionConfig, DbConnection, MySql}

//useful for a quick test, like
// Test / runMain com.colisweb.db.diff.DbRawSql <password> "select * from deliveries"
object DbRawSql extends App {
  val List(password, query) = args.toList

  val connection = DbConnection(
    ConnectionConfig(
      url = "jdbc:p6spy:mysql://localhost:25433/apiv2_production",
      user = "colisweb",
      password = password,
      MySql,
      useCachedMetadata = true
    ),
    service = "test",
    environment = "local"
  )

  def run() = {
    val iterator = connection.repository.sqlSelect(query)
    println(iterator.columns)
    iterator.batch(1000).take(5).foreach(d => println(d.data))
  }
  run()
  run()
}
