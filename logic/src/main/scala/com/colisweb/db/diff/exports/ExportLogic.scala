package com.colisweb.db.diff.exports

import com.colisweb.db.diff.db.metadata._
import com.colisweb.db.diff.db.{DataRows, DbConnection}
import org.slf4j.LoggerFactory

import scala.annotation.tailrec

final case class ExportLogic(
    dbConnection: DbConnection,
    exportConfig: ExportConfig
) {
  import dbConnection._

  val links: Seq[TableLink] =
    GuessedMetadata(
      standardDbMetadata = metadata,
      skippedLinks = exportConfig.skippedLinks,
      skippedTables = exportConfig.connection.skippedTables,
      guessMissingForeignKeys = exportConfig.guessMissingForeignKeys
    ).links

  case class ExportLogicForTable(tableName: String) {
    val tableConfig: Option[TableConfig] = exportConfig.tableConfig(tableName)
    val tableMetaData: TableMetaData     = metadata.allTables(tableName)

    val logger = LoggerFactory.getLogger(getClass)

    def readTableRecursively(): Seq[DataRows] = {
      @tailrec
      def readRec(newRows: List[DataRows], acc: Seq[DataRows], explored: Set[ExploredData] = Set.empty): Seq[DataRows] =
        newRows match {
          case head :: tail if head.size == 0 =>
            logger.debug(s"skipping empty ${head.tableName}")
            readRec(tail, acc, explored)
          case head :: tail                   =>
            logger.info(s"exploring ${head.tableName} (${head.data.size} rows), with ${tail.map(_.tableName)} next")
            val (nextRows, newExplored) =
              ExportLogicForTable(head.tableName).followLinks(explored)(head).unzip
            readRec(
              DataRows.aggregate(nextRows ++ tail),
              DataRows.aggregate(acc ++ nextRows),
              explored ++ newExplored.flatten
            )
          case Nil                            => acc
        }

      val values =
        if (exportConfig.tableConfig(tableName).exists(_.followKeys))
          readRec(List(tableContent), Seq(tableContent), explored.toSet)
        else Seq(tableContent)

      for { rows <- values } logger.info(
        s"${rows.data.size} rows from ${rows.tableName} (links from ${rows.parentTables.mkString(",")})"
      )
      values.map(replaceForAnonymity)
    }

    lazy val explored: Seq[ExploredData] = tableMetaData.columns
      .find(c => tableMetaData.primaryKeyColumns.exists(pk => pk.toLowerCase == c.name.toLowerCase))
      .fold(Vector.empty[ExploredData])(i =>
        tableContent.map(i.name).flatten.map(v => ExploredData(tableName, i.name, v))
      )

    lazy val tableContent: DataRows = {
      val maybeFilterOnIds = for {
        tableConfig <- exportConfig.tableConfig(tableName)
        id          <- tableConfig.idColumnName
        if tableConfig.idValues.nonEmpty
        column      <- columns.find(_.name.toLowerCase == id.toLowerCase)
      } yield (column, tableConfig.idValues)

      val columnFilter = maybeFilterOnIds.map {
        case (cd, values) => ColumnFilter(cd, values)
      }.toList
      repository.select(
        columns.map(_.name),
        TableFilter(tableName, null, columnFilter),
        tableConfig,
        tableMetaData
      )
    }

    private def followLinks(explored: Set[ExploredData])(dataRows: DataRows): Seq[(DataRows, Set[ExploredData])] =
      if (!dataRows.isEmpty) {
        links
          .collect {
            case link @ TableLink(from @ dataRows.relevant(rows), to, _) =>
              logger.info(s"follow link $from -> $to")
              ExportLogicForTable(to.table).followLink(rows, link, explored)
          }
      } else Nil

    private def followLink(
        originals: DataRows,
        link: TableLink,
        explored: Set[ExploredData]
    ): (DataRows, Set[ExploredData]) = {
      val column       = columns
        .find(_.name == link.to.idColumn)
        .getOrElse(throw new RuntimeException(s"Unknown column ${link.from.idColumn} in $tableName"))
      val askedValues  = originals.map(link.from.idColumn).flatten.distinct.filter("null".!=)
      val neededValues = askedValues.filter(v => !explored(ExploredData(tableName, column.name, v)))
      val found        =
        if (neededValues.nonEmpty)
          repository
            .select(
              columns.map(_.name),
              link.to.withValues(column, neededValues),
              tableConfig,
              tableMetaData
            )
            .copy(parentTables = List(originals.tableName))
        else DataRows(tableName)
      logger.info(
        s"${found.data.size} rows in $tableName where ${column.name} in (${neededValues.take(10).mkString(",")})"
      )
      (found, neededValues.toSet.map((v: String) => ExploredData(table = tableName, key = column.name, value = v)))
    }

    private def columns: Seq[ColumnDescription] =
      (exportConfig.tableConfig(tableName) match {
        case Some(tableConfig) if tableConfig.skipped.nonEmpty =>
          tableMetaData.columns.filterNot(c => tableConfig.skipped(c.name))
        case _                                                 =>
          tableMetaData.columns
      }).filterNot(c => exportConfig.skippedColumns(c.name))

  }

  private def replaceForAnonymity(rows: DataRows): DataRows = {
    exportConfig
      .tableConfig(rows.tableName)
      .fold(Seq.empty[(String, String)])(_.columns.collect {
        case ColumnConfig(name, Some(replacement), _) => name -> replacement
      })
      .foldLeft(rows) {
        case (row, (columnName, replacement)) => row.replace(columnName, replacement)
      }
  }

}

case class KeyToFollow(table: String, idInTable: String, fromId: String)

case class ExploredData(table: String, key: String, value: String)
